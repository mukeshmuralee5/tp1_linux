# TP2 pt. 2 Linux


## 2. Setup

- le README.md y est souvent très complet
- présente la solution, et les étapes d'install
- fournit les liens vers la doc

**Setup Netdata**

#opt/netdata/etc/netdata
Sur toutes les machines que vous souhaitez monitorer :

```
[admin@web ~]$ sudo su -
Last login: Wed Oct  6 16:14:52 CEST 2021 on pts/1
[root@web ~]# bash <(curl -Ss https://my-netdata.io/kickstart-static64.sh)
 [...]

  Your netdata configuration will be retained.
  After installation, netdata will be (re-)started.

  netdata re-distributes a lot of open source software components.
  Check its full license at:
  https://github.com/netdata/netdata/blob/master/LICENSE.md
Please type y to accept, n otherwise: y
[...]

  ^
  |.-.   .-.   .-.   .-.   .-.   .  netdata              .-.   .-.   .-.   .-
  |   '-'   '-'   '-'   '-'   '-'   is installed now!  -'   '-'   '-'   '-'
  +----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+--->

[/opt/netdata]# chmod 0644 /opt/netdata/etc/netdata/netdata.conf
 OK

 OK

[/tmp/netdata-kickstart-FEK5SzuIXr]# rm /tmp/netdata-kickstart-FEK5SzuIXr/netdata-latest.gz.run
 OK

[/tmp/netdata-kickstart-FEK5SzuIXr]# rm -rf /tmp/netdata-kickstart-FEK5SzuIXr

```

**Manipulation du *service* Netdata**

- déterminer s'il est actif, et s'il est paramétré pour démarrer au boot de la machine

````

[admin@web ~]$ sudo systemctl status netdata
[sudo] password for admin:
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-11 15:11:55 CEST; 8min ago
  [...]
  
  
[admin@web ~]$ sudo systemctl is-enabled netdata
enabled
````

- déterminer à l'aide d'une commande `ss` sur quel port Netdata écoute
````
[admin@web ~]$ sudo ss -alpnt
[sudo] password for admin:
State     Recv-Q    Send-Q       Local Address:Port         Peer Address:Port    Process
LISTEN    0         128                0.0.0.0:22                0.0.0.0:*        users:(("sshd",pid=755,fd=5))
LISTEN    0         128              127.0.0.1:8125              0.0.0.0:*        users:(("netdata",pid=2225,fd=35))
LISTEN    0         128                0.0.0.0:19999             0.0.0.0:*        users:(("netdata",pid=2225,fd=5))
LISTEN    0         128                   [::]:22                   [::]:*        users:(("sshd",pid=755,fd=7))
LISTEN    0         128                  [::1]:8125                 [::]:*        users:(("netdata",pid=2225,fd=34))
LISTEN    0         128                   [::]:19999                [::]:*        users:(("netdata",pid=2225,fd=6))
LISTEN    0         128                      *:80                      *:*        users:(("httpd",pid=1645,fd=4),("httpd",pid=782,fd=4),("httpd",pid=781,fd=4),("httpd",pid=780,fd=4),("httpd",pid=756,fd=4))
````

- autoriser ce port dans le firewall

````
[admin@web ~]$ sudo firewall-cmd --add-port=19999/tcp
[sudo] password for admin:
success

[admin@db ~]$ sudo firewall-cmd --reload
success
````


**Setup Alerting**

- ajuster la conf de Netdata pour mettre en place des alertes Discord

````
[admin@db ~]$ sudo /opt/netdata//etc/netdata/edit-config health_alarm_notify.conf

[...]

SEND_DISCORD="YES"

DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/897134603624661072/uY-3pEocXZaK6m0yvBsw17MD59WjUa3-hjYO2Z0djhDdKOhZZMx9GtLbHWWFBsDf1v2H"

DEFAULT_RECIPIENT_DISCORD="alarms"

````


- Vérifiez le bon fonctionnement de l'alerting sur Discord

````
[admin@db ~]$ sudo su -s /bin/bash netdata
bash-4.4$ export NETDATA_ALARM_NOTIFY_DEBUG=1
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test

# SENDING TEST WARNING ALARM TO ROLE: sysadmin
[...]
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-11 16:42:19: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
bash-4.4$ /opt/netdata/usr/libexec/netdata/plugins.d/alarm-notify.sh test "ROLE"

# SENDING TEST WARNING ALARM TO ROLE: ROLE
[...]
--- END received response ---
RECEIVED HTTP RESPONSE CODE: 200
2021-10-11 16:42:31: alarm-notify.sh: INFO: sent discord notification for: db.tp2.linux test.chart.test_alarm is CLEAR to 'alarms'
# OK
bash-4.4$
```` 


# II. Backup

| Machine            | IP            | Service                 | 
|--------------------|---------------|-------------------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) |


## 2. Partage NFS

**Setup environnement**

- créer un dossier `/srv/backup/` 
  
  ```` 
  [admin@backup ~]$ sudo mkdir /srv/backup/
  [admin@backup ~]$ sudo ls /srv
  backup
  ````


- créer un sous-dossier pour chaque machine du parc
  ````
  [admin@backup ~]$ sudo mkdir /srv/backup/web.tp2.linux/
  [admin@backup ~]$ sudo mkdir /srv/backup/db.tp2.linux/
  [admin@backup ~]$ sudo ls /srv/backup/
  db.tp2.linux  web.tp2.linux
  ````
  - commencez donc par créer le dossier `/srv/backup/web.tp2.linux/`
- il existera un partage NFS pour chaque machine (principe du moindre privilège)

**Setup partage NFS**

*backup.tp2.linux*
````
[admin@backup ~]$ sudo dnf install -y nfs-utils
[...]
Complete!

[admin@backup ~]$ sudo vim /etc/exports
[admin@backup ~]$ sudo cat /etc/exports
/srv/backup/web.tp2.linux 10.102.1.11(rw,no_root_squash)
/srv/backup/db.tp2.linux 10.102.1.12(rw,no_root_squash)

[admin@backup ~]$ Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

[admin@backup ~]$ sudo systemctl is-active nfs-server
[sudo] password for admin:
active

[admin@backup ~]$ sudo systemctl is-enabled nfs-server
enabled
````

*web.tp2.linux*
````
[admin@web ~]$ sudo systemctl enable --now rpcbind nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.

[admin@web ~]$ sudo systemctl is-enabled nfs-server
enabled

[admin@web ~]$ sudo systemctl is-active nfs-server
active
````

**Setup points de montage sur `web.tp2.linux`**

  ```` 
  [admin@web ~]$ sudo mount -t nfs backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup
  [admin@web ~]$ sudo df -h
  Filesystem                                  Size  Used Avail Use% Mounted on
  devtmpfs                                    867M     0  867M   0% /dev
  tmpfs                                       885M  364K  885M   1% /dev/shm
  tmpfs                                       885M  8.6M  877M   1% /run
  tmpfs                                       885M     0  885M   0% /sys/fs/cgroup
  /dev/mapper/rl-root                         6.2G  3.8G  2.5G  61% /
  /dev/sda1                                  1014M  241M  774M  24% /boot
  tmpfs                                       177M     0  177M   0% /run/user/1000
  backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.2G  4.0G  36% /srv/backup
  
  
  [admin@web ~]$ sudo mount
  [...]
  backup.tp2.linux:/srv/backup/web.tp2.linux on /srv/backup type nfs4 (rw,relatime, vers=4.2,rsize=262144,wsize=262144,namlen=255,hard,proto=tcp,timeo=600,retrans=2,  sec=sys,clientaddr=10.102.1.11,local_lock=none,addr=10.102.1.13)
  
  [admin@web ~]$ sudo df -h
  [sudo] password for admin:
  Filesystem                                  Size  Used Avail Use% Mounted on
  devtmpfs                                    867M     0  867M   0% /dev
  tmpfs                                       885M  364K  885M   1% /dev/shm
  tmpfs                                       885M  8.6M  877M   1% /run
  tmpfs                                       885M     0  885M   0% /sys/fs/cgroup
  /dev/mapper/rl-root                         6.2G  3.8G  2.5G  61% /
  /dev/sda1                                  1014M  241M  774M  24% /boot
  tmpfs                                       177M     0  177M   0% /run/user/1000
  backup.tp2.linux:/srv/backup/web.tp2.linux  6.2G  2.2G  4.0G  36% /srv/backup
  
  [admin@web ~]$ ls /srv/backup/
  test.txt
  [admin@web ~]$ vim /srv/backup/test.txt
  [admin@web ~]$ cat /srv/backup/test.txt
  oui oui oui
  [admin@web ~]$ touch /srv/backup/test2.txt
  [admin@web ~]$ ls /srv/backup/
  test2.txt  test.txt

  [admin@web ~]$ sudo vim /etc/fstab
  [admin@web ~]$ sudo cat /etc/fstab
    [...]
  /dev/mapper/rl-root     /                       xfs     defaults        0 0
  UUID=40dc4232-ea1c-4ed0-ae70-0d89d1f845fe /boot                   xfs     defaults        0 0
  /dev/mapper/rl-swap     none                    swap    defaults        0 0
  backup.tp2.linux:/srv/backup/web.tp2.linux /srv/backup        nfs     defaults        0 0
  
  ````
  
## 3. Backup de fichiers


**Rédiger le script de backup `/srv/tp2_backup.sh`**

```
[admin@web ~]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Simple backup script
# Admin ~ 12/10/21

  DATE=$(date +%y%m%d_%H%M%S)
  Destination=$1
  Target=$2
  echo ${Target}
  Here=$(pwd)/"tp2_backup_${DATE}.tar.gz"
  tar cvzf "tp2_backup_${DATE}.tar.gz" ${Target}
  Qty=5
  rsync -av --remove-source-files ${Here} ${Destination}
  ls -tp "${Destination}" | grep -v '/$' | tail -n +6 | xargs -I {} rm -- ${Destination}/{}

```

📁 **Fichier [`/srv/tp2_backup.sh`] https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup.sh

**Tester le bon fonctionnement**
```` 
[admin@web ~]$ ls Backup/
[admin@web ~]$ sudo vim /srv/tp2_backup.sh
[admin@web ~]$ sudo bash -x /srv/tp2_backup.sh BackupTest/ test/
++ date +%y%m%d_%H%M%S
+ DATE=211015_151838
+ Destination=test/
+ Target=BackupTest/
+ echo BackupTest/
BackupTest/
++ pwd
+ Here=/home/admin/tp2_backup_211015_151838.tar.gz
+ tar cvzf tp2_backup_211015_151838.tar.gz test/
test/
test/testtest
+ rsync -av --remove-source-files /home/admin/tp2_backup_211015_151838.tar.gz BackupTest/
sending incremental file list
tp2_backup_211015_151838.tar.gz

sent 274 bytes  received 43 bytes  634.00 bytes/sec
total size is 157  speedup is 0.50
[admin@web ~]$ ls
 BackupTest  'q!'   test   testtest
 
[admin@web ~]$ cd BackupTest/
[admin@web BackupTest]$ ls
tp2_backup_211015_151838.tar.gz
[admin@web BackupTest]$ sudo tar xzvf tp2_backup_211015_151838.tar.gz
test/
test/testtest

[admin@web BackupTest]$ ls
test  tp2_backup_211015_151838.tar.gz

[admin@web BackupTest]$ cd test/
[admin@web test]$ ls
testtest

[admin@web test]$ cat testtest
Lorem Ipsum
```` 

## 4. Unité de service


### A. Unité de service

**Créer une *unité de service*** pour notre backup

```
[admin@web ~]$ sudo vim /etc/systemd/system/tp2_backup.service
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=sudo bash /srv/tp2_backup.sh /home/admin/BackupTest/ /home/admin/test/
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

### B. Timer

Un *timer systemd* permet l'exécution d'un *service* à intervalles réguliers.

**Créer le *timer* associé à notre `tp2_backup.service`**


```
[admin@web ~]$ sudo vim /etc/systemd/system/tp2_backup.timer
[...]
Description=Periodically run our TP2 backup script
Requires=tp2_backup.service

[Timer]
Unit=tp2_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

**Activez le timer**
```
[admin@web ~]$ sudo systemctl start tp2_backup.timer
[admin@web ~]$ sudo systemctl enable tp2_backup.timer
[admin@web ~]$ sudo systemctl status tp2_backup.timer
● tp2_backup.timer
   Loaded: loaded (/etc/systemd/system/tp2_backup.timer; disabled; vendor preset: disabled)
   Active: active (waiting) since Fri 2021-10-15 17:07:22 CEST; 5s ago
  [...]
```

**Tests !**

```
[admin@web ~]$ ls BackupTest/
test  tp2_backup_211015_151838.tar.gz  tp2_backup_211015_153207.tar.gz  tp2_backup_211015_153822.tar.gz
[admin@web ~]$ ls BackupTest/
test  tp2_backup_211015_151838.tar.gz  tp2_backup_211015_153207.tar.gz  tp2_backup_211015_153822.tar.gz  tp2_backup_211015_170801.tar.gz
```
---

### C. Contexte

- le dossier sauvegardé est celui qui contient le site NextCloud (quelque part dans `/var/`)

```
[admin@web ~]$ sudo systemctl list-timers
NEXT                          LEFT     LAST                          PASSED       UNIT                         ACTIVATES
Sat 2021-10-16 03:15:00 CEST  9h left  n/a                           n/a          tp2_backup.timer             tp2_backup.service
Sat 2021-10-16 14:30:21 CEST  20h left Fri 2021-10-15 14:30:21 CEST  3h 32min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service
n/a                           n/a      n/a                           n/a          dnf-makecache.timer          dnf-makecache.service

3 timers listed.
Pass --all to see loaded but inactive timers, too.
```

📁 **Fichier [`/etc/systemd/system/tp2_backup.timer`] https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup.timer

📁 **Fichier [`/etc/systemd/system/tp2_backup.service`] https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup.service


## 5. Backup de base de données

Sauvegarder des dossiers c'est bien. Mais sauvegarder aussi les bases de données c'est mieux.

**Création d'un script `/srv/tp2_backup_db.sh`**

```
[admin@db ~]$ sudo bash -x /srv/tp2_backup_db.sh TestBackDB/ nextCloud

+ '[admin@web' '~]$' sudo cat /srv/tp2_backup.sh
/srv/tp2_backup_db.sh: line 1: [admin@web: command not found
++ date +%y%m%d_%H%M%S
+ DATE=211022_103412
+ Destination=TestBackDB/
+ Target=nextCloud
++ pwd
+ Here=/home/admin/tp2_backup_211022_103412.tar.gz
+ mysqldump --user=wb --password=admin --databases nextCloud
mysqldump: Got error: 1045: "Access denied for user 'wb'@'localhost' (using password: YES)" when trying to connect
+ tar cvzf tp2_backup_211022_103412.tar.gz backup_donnees.sql
backup_donnees.sql
+ rsync -av --remove-source-files /home/admin/tp2_backup_211022_103412.tar.gz TestBackDB/
sending incremental file list
tp2_backup_211022_103412.tar.gz

sent 239 bytes  received 43 bytes  564.00 bytes/sec
total size is 123  speedup is 0.44
+ rm -rf backup_donnees.sql
+ tail -n +6
+ ls -tp TestBackDB/
+ grep -v '/$'
+ xargs -I '{}' rm -- 'TestBackDB//{}'
[admin@db ~]$ ls TestBackDB/
tp2_backup_211022_103412.tar.gz
[admin@db ~]$
[admin@db ~]$ ls TestBackDB/
tp2_backup_211022_103412.tar.gz 
```

📁 **Fichier [`/srv/tp2_backup_db.sh`] https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup_db.sh

🌞 **Restauration**


***Unité de service***

- création service tp2_backup_db.service:
  ```
  [admin@db ~]$ sudo cat /etc/systemd/system/tp2_backup_db.service
  [Unit]
  Description=Our own lil backup service (TP2)

  [Service]
  ExecStart=sudo bash /srv/tp2_backup_db.sh /home/admin/TestBackDB/ nextCloud
  Type=oneshot
  RemainAfterExit=no

  [Install]
  WantedBy=multi-user.target
  [admin@db ~]$ sudo systemctl start tp2_backup_db.service
  [admin@db ~]$ ls TestBackDB/
  tp2_backup_211022_103412.tar.gz  tp2_backup_211022_130838.tar.gz
  ```
- création et activation du timer tp2_backup_db.timer

  ```
  [admin@db ~]$ sudo cat /etc/systemd/system/tp2_bachup_db.timer
  Description=Periodically run our TP2 backup db script
  Requires=tp2_backup_db.service

  [Timer]
  Unit=tp2_backup_db.service
  OnCalendar=*-*-* 3:30:00

  [Install]
  WantedBy=timers.target
  [admin@db ~]$ sudo systemctl daemon-reload
  [admin@db ~]$ sudo systemctl start tp2_backup_db.timer
  [admin@db ~]$ sudo systemctl enable tp2_backup_db.timer
  Created symlink /etc/systemd/system/timers.target.wants/tp2_backup_db.timer → /etc/systemd/system/tp2_backup_db.timer.
  [admin@db ~]$ sudo systemctl is-enabled tp2_backup_db.timer
  enabled
  [admin@db ~]$ sudo systemctl is-active tp2_backup_db.timer
  active
  [admin@db ~]$ sudo systemctl list-timers
  NEXT                          LEFT     LAST                          PASSED   UNIT                   >
  Sat 2021-10-23 03:30:00 CEST  14h left n/a                           n/a      tp2_backup_db.timer    >
  Sat 2021-10-23 13:16:24 CEST  23h left Fri 2021-10-22 13:16:24 CEST  5min ago systemd-tmpfiles-clean.>
  n/a                           n/a      n/a                           n/a      dnf-makecache.timer    >
  3 timers listed.
  Pass --all to see loaded but inactive timers, too.
  lines 1-7/7 (END)
  ```

📁 **Fichier [`/etc/systemd/system/tp2_backup_db.timer`]**  https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup_db.timer

📁 **Fichier [`/etc/systemd/system/tp2_backup_db.service`]** https://gitlab.com/mukeshmuralee5/tp1_linux/-/blob/main/TP2_p2/tp2_backup_db.service

## 6. Petit point sur la backup

### 2. Setup simple

Installation sur le front: 

```
[admin@front ~]$ sudo dnf install -y epel-release
Rocky Linux 8 - AppStream                                                               4.3 kB/s | 4.8 kB     00:01
Rocky Linux 8 - AppStream
[...]
Complete!
[admin@front ~]$ sudo dnf install -y nginx
Last metadata expiration check: 0:00:33 ago on Fri 22 Oct 2021 11:26:36 AM CEST.
Dependencies resolved.
[...]
Complete!
```

Mise en place du servcie:

```
[admin@front ~]$ sudo systemctl start nginx.service
[admin@front ~]$ sudo systemctl enable nginx.service
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[admin@front ~]$ sudo ss -alpnt | grep nginx
LISTEN 0      128          0.0.0.0:80         0.0.0.0:*    users:(("nginx",pid=5447,fd=8),("nginx",pid=5446,fd=8))
LISTEN 0      128             [::]:80            [::]:*    users:(("nginx",pid=5447,fd=9),("nginx",pid=5446,fd=9))
[admin@front ~]$ sudo firewall-cmd --add-port=80/tcp
success
[admin@front ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[admin@front ~]$ sudo firewall-cmd --reload
success
[admin@front ~]$
```
On peut vérifier son activation en allant dans notre navigateur et en recherchant l'adresse IP de notre vm (`10.102.1.14`).

Fichier conf: 

  - Nom d'utilisateur:
  ``` 
  [...] 
  user nginx;
  [...]
  ```

  - Bloc ``server``:
  ``` 
  server {
    listen       80 default_server;
    listen       [::]:80 default_server;
    server_name  _;
    root         /usr/share/nginx/html;

    # Load configuration files for the default server block.
    include /etc/nginx/default.d/*.conf;

    location / {
    }

    error_page 404 /404.html;
        location = /40x.html {
    }

    error_page 500 502 503 504 /50x.html;
        location = /50x.html {
    }
  }
  ```

  - Fichier conf inclus :
  ````
  include /usr/share/nginx/modules/*.conf;
  [...]
    include             /etc/nginx/mime.types;
  [...]
    include /etc/nginx/conf.d/*.conf;
  [...]
        include /etc/nginx/default.d/*.conf;
  [...]
  ````

Après suppression du bloc server, création du fichier : `/etc/nginx/conf.d/web.tp2.linux.conf`

```
[admin@front ~]$ sudo cat /etc/nginx/conf.d/web.tp2.linux.conf
server {
    # on demande à NGINX d'écouter sur le port 80 pour notre NextCloud
    listen 80;

    # ici, c'est le nom de domaine utilisé pour joindre l'application
    # ce n'est pas le nom du reverse proxy, mais le nom que les clients devront saisir pour atteindre le site
    server_name web.tp2.linux; # ici, c'est le nom de domaine utilisé pour joindre l'application (pas forcéme

    # on définit un comportement quand la personne visite la racine du site (http://web.tp2.linux/)
    location / {
        # on renvoie tout le trafic vers la machine web.tp2.linux
        proxy_pass http://web.tp2.linux;
    }
}
```

## IV. Firewalling

### 2. Mise en place

#### A. Base de données

**Restriction de l'accès à la base de données `db.tp2.linux`**
Accès limité par ssh à notre hote:

```

[admin@db ~]$ sudo firewall-cmd  --set-default-zone=drop
success
[admin@db ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8
Warning: ZONE_ALREADY_SET: 'enp0s8' already bound to 'drop'
success
[admin@db ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[admin@db ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success

```

Accès limité à notre service web:

```
[admin@db ~]$ sudo firewall-cmd --zone=db --add-source=10.102.1.11/32 --permanent
success
[admin@db ~]$ sudo firewall-cmd --zone=db --add-port=3306/tcp --permanent
success
```

**Démonstration du bon fonctionnement de notre configuration**

```
[admin@db ~]$ sudo firewall-cmd --get-active-zones
db
  sources: 10.102.1.11/32
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
  
[admin@db ~]$ sudo firewall-cmd --get-default-zone
drop

[admin@db ~]$ sudo firewall-cmd --list-all --zonedb
usage: see firewall-cmd man page
firewall-cmd: error: unrecognized arguments: --zonedb

[admin@db ~]$ sudo firewall-cmd --list-all --zone=db
db (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.11/32
  ports: 3306/tcp
  masquerade: no
 
[admin@db ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  icmp-block-inversion: no
  sources: 10.102.1.1/32
  ports: 22/tcp
  masquerade: no

[admin@db ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  masquerade: no
```

### B. Serveur Web

🌞 **Restreindre l'accès au serveur Web `web.tp2.linux`**

- seul le reverse proxy `front.tp2.linux` doit accéder au serveur web sur le port 80
- n'oubliez pas votre accès SSH

**Restriction de l'accès au serveur Web `web.tp2.linux`**

Accès limité par ssh à notre hote et mise en place du `drop` par défaut:
```
[admin@web ~]$ sudo firewall-cmd --set-default-zone=drop
success
[admin@web ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success
[admin@web ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[admin@web ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
```

Accès limité au `front.tp2.linux`:

```
[admin@web ~]$ sudo firewall-cmd --zone=web --add-source=10.102.1.14/32 --permanent
success
[admin@web ~]$ sudo firewall-cmd --zone=web --add-port=80/tcp --permanent
success
```


**Démonstration du bon fonctionnement de notre configuration**

```
[admin@web ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
ssh
  sources: 10.102.1.1/32
web
  sources: 10.102.1.14/32
[admin@web ~]$ sudo firewall-cmd --get-default-zone
drop
[admin@web ~]$ sudo firewall-cmd --list-all --zone=web
web (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.14/32
  ports: 80/tcp
  masquerade: no
  [...]
  
[admin@web ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.1/32
  ports: 22/tcp
  masquerade: no
  [...]
  
[admin@web ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  masquerade: no
 [...]
 
```

### C. Serveur de backup

**Restriction d'accès au serveur de backup `backup.tp2.linux`**

Mise en place du drop par défaut:
```
[admin@backup ~]$ sudo firewall-cmd --set-default-zone=drop
[sudo] password for admin:
success
[admin@backup ~]$ sudo firewall-cmd --zone=drop --add-interface=enp0s8 --permanent
The interface is under control of NetworkManager, setting zone to 'drop'.
success
```

Accès limité par ssh à l'hôte:
```
[admin@backup ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[admin@backup ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
```

Accès limité aux machines utilisant le service nfs:
```
[admin@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.11/32 --permanent
success
[admin@backup ~]$ sudo firewall-cmd --zone=nfs --add-source=10.102.1.12/32 --permanent
success
[admin@backup ~]$ sudo firewall-cmd --zone=nfs --add-port=19999/tcp --permanent
success
[admin@backup ~]$ sudo firewall-cmd --zone=nfs --add-service=nfs --permanent
success
```

**Démonstration du bon fonctionnement de notre configuration**
```
[admin@backup ~]$ sudo firewall-cmd --get-active-zones
[sudo] password for admin:
drop
  interfaces: enp0s8 enp0s3
nfs
  sources: 10.102.1.11/32 10.102.1.12/32
ssh
  sources: 10.102.1.1/32
[admin@backup ~]$ sudo firewall-cmd --get-default-zone
drop
[admin@backup ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.1/32
  ports: 22/tcp
  masquerade: no
    [...]

[admin@backup ~]$ sudo firewall-cmd --list-all --zone=nfs
nfs (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.11/32 10.102.1.12/32
  services: nfs
  ports: 19999/tcp
  masquerade: no
  [...]
  
[admin@backup ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  masquerade: no

```

### D. Reverse Proxy

**Restreindre l'accès au reverse proxy `front.tp2.linux`**


```
[admin@front ~]$ sudo firewall-cmd --set-default-zone=drop
success
[admin@front ~]$ sudo firewall-cmd --zone=drop --permanent --add-interface=enp0s8
success

```

Accès limité en ssh à l'hôte:
```
[admin@front ~]$ sudo firewall-cmd --zone=ssh --add-source=10.102.1.1/32 --permanent
success
[admin@front ~]$ sudo firewall-cmd --zone=ssh --add-port=22/tcp --permanent
success
```

Accès limité pour tout les réseau:
```
[admin@front ~]$ sudo firewall-cmd --new-zone=proxy --permanent
success
[admin@front ~]$ sudo firewall-cmd --zone=proxy --add-source=10.102.1.0/24 --permanent
success
[admin@front ~]$ sudo firewall-cmd --reload
success
```

**Démonstration du bon fonctionnement de notre configuration**


```
[admin@front ~]$ sudo firewall-cmd --get-active-zones
drop
  interfaces: enp0s8 enp0s3
proxy
  sources: 10.102.1.0/24
ssh
  sources: 10.102.1.1/32
  
[admin@front ~]$ sudo firewall-cmd --get-default-zone
drop

[admin@front ~]$ sudo firewall-cmd --list-all --zone=drop
drop (active)
  target: DROP
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
[...]

[admin@front ~]$ sudo firewall-cmd --list-all --zone=ssh
ssh (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.1/32
  ports: 22/tcp
  masquerade: no
[...]
    
[admin@front ~]$ sudo firewall-cmd --list-all --zone=proxy
proxy (active)
  target: default
  icmp-block-inversion: no
  sources: 10.102.1.0/24
  masquerade: no
[...]
```

### E. Tableau récap


| Machine            | IP            | Service                 | Port ouvert | IPs autorisées |
|--------------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux`    | `10.102.1.11` | Serveur Web             | 80          | 10.102.14     |
| `db.tp2.linux`     | `10.102.1.12` | Serveur Base de Données | 3306        | 10.102.1.11   |
| `backup.tp2.linux` | `10.102.1.13` | Serveur de Backup (NFS) | 19999       | 10.102.1.11 & 10.102.1.12               |
| `front.tp2.linux`  | `10.102.1.14` | Reverse Proxy           | 443         | de 10.102.1.1 à 10.102.1.254            |
