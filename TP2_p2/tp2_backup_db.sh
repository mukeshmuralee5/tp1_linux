[tamashi@web ~]$ sudo cat /srv/tp2_backup.sh
#!/bin/bash
# Sauvegarde de base de données
# Tamashi - 13/10/2021

  DATE=$(date +%y%m%d_%H%M%S)
  Destination=$1
  Target=$2
  Here=$(pwd)/"tp2_backup_${DATE}.tar.gz"

  mysqldump --user=wb --password=tamashi --databases ${Target} > backup_donnees.sql

  tar cvzf "tp2_backup_${DATE}.tar.gz" backup_donnees.sql
  rsync -av --remove-source-files ${Here} ${Destination}
  rm -rf backup_donnees.sql
  ls -tp "${Destination}" | grep -v '/$' | tail -n +6 | xargs -I {} rm -- ${Destination}/{}
