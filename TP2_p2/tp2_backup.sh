#!/bin/bash
# Simple backup script
# Tamashi ~ 12/10/21

  DATE=$(date +%y%m%d_%H%M%S)
  Destination=$1
  Target=$2
  echo ${Target}
  Here=$(pwd)/"tp2_backup_${DATE}.tar.gz"
  tar cvzf "tp2_backup_${DATE}.tar.gz" ${Target}
  Qty=5
  rsync -av --remove-source-files ${Here} ${Destination}
  ls -tp "${Destination}" | grep -v '/$' | tail -n +6 | xargs -I {} rm -- ${Destination}/{}

