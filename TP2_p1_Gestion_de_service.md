# TP2 pt. 1 : Gestion de service


## 1. Un premier serveur web



### 🌞 Installer le serveur Apache

```
[admin@web ~]$ sudo yum install httpd

Rocky Linux 8 - AppStream                                    5.9 kB/s | 4.8 kB     00:00
Rocky Linux 8 - AppStream                                    3.5 MB/s | 9.1 MB     00:02
Rocky Linux 8 - BaseOS                                        13 kB/s | 4.3 kB     00:00
    [...]

```



## 🌞 Démarrer le service Apache

```
[admin@web ~]$ systemctl start httpd
```

### Faites en sorte qu'Apache démarre automatique au démarrage de la machine

```
[admin@web ~]$ systemctl enable httpd.service*


[admin@web ~]$ sudo systemctl status httpd
[sudo] password for admin:
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2021-10-10 12:50:31 CEST; 4h 2min ago
     Docs: man:httpd.service(8)
  Process: 1768 ExecReload=/usr/sbin/httpd $OPTIONS -k graceful (code=exited, status=0/SUCCE>
 Main PID: 746 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 11398)
   Memory: 28.5M
   CGroup: /system.slice/httpd.service
           ├─ 746 /usr/sbin/httpd -DFOREGROUND
           ├─1775 /usr/sbin/httpd -DFOREGROUND
           ├─1776 /usr/sbin/httpd -DFOREGROUND
           ├─1777 /usr/sbin/httpd -DFOREGROUND
           └─1778 /usr/sbin/httpd -DFOREGROUND

Oct 10 12:50:29 localhost.localdomain systemd[1]: Starting The Apache HTTP Server...
Oct 10 12:50:31 localhost.localdomain httpd[746]: AH00558: httpd: Could not reliably determi>
Oct 10 12:50:31 localhost.localdomain systemd[1]: Started The Apache HTTP Server.
Oct 10 12:50:31 localhost.localdomain httpd[746]: Server configured, listening on: port 80
Oct 10 15:16:01 localhost.localdomain systemd[1]: Reloading The Apache HTTP Server.
Oct 10 15:16:01 localhost.localdomain httpd[1768]: AH00558: httpd: Could not reliably determ>
Oct 10 15:16:01 localhost.localdomain systemd[1]: Reloaded The Apache HTTP Server.
Oct 10 15:16:01 localhost.localdomain httpd[746]: Server configured, listening on: port 80
lines 1-24/24 (END)
```

    
### Utiliser une commande ss pour savoir sur quel port tourne actuellement Apache 

```
[admin@web ~]$ sudo firewall-cmd --add-port 80/tcp --permanent
[sudo] password for admin:
Warning: ALREADY_ENABLED: 80:tcp
success
```

###  🌞 TEST

### vérifier que le service est démarré & vérifier qu'il est configuré pour démarrer automatiquement

```
[admin@web ~]$ sudo systemctl list-unit-files --state=enabled
[sudo] password for admin:
UNIT FILE                                  STATE
[...]
httpd.service                              enabled
[...]

```

### vérifier avec une commande curl localhost que vous joignez votre serveur web localement

```

[admin@web ~]$ curl localhost
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
        height: 100%;
        width: 100%;
      }
        body {
  background: rgb(20,72,50);
 
        [...]
```

### vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web

![](https://i.imgur.com/dBkmnyK.png)


## 2. Avancer vers la maîtrise du service

### Donnez la commande qui permet d'activer le démarrage automatique d'Apache quand la machine s'allume 

```
[admin@web ~]$ systemctl enable httpd.service
```
### Prouvez avec une commande qu'actuellement, le service est paramétré pour démarré quand la machine s'allume

```
[admin@web ~]$ sudo systemctl is-enabled httpd
[sudo] password for admin:
enabled
```

### Affichez le contenu du fichier httpd.service qui contient la définition du service Apache


```
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```


## 🌞 Déterminer sous quel utilisateur tourne le processus Apache

```
[admin@web ~]$ cat /etc/httpd/conf/httpd.conf

ServerRoot "/etc/httpd"

Listen 80

Include conf.modules.d/*.conf

User apache
```
### utilisez la commande ps -ef pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```
[admin@web ~]$ ps -ef
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
apache       785     756  0 09:29 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       787     756  0 09:29 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
apache       788     756  0 09:29 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
apache       789     756  0 09:29 ?        00:00:02 /usr/sbin/httpd -DFOREGROUND
[...]
```
### vérifiez avec un ls -al que tout son contenu est accessible en lecture à l'utilisateur mentionné dans le fichier de conf
```
[admin@web ~]$ ls -la
total 16
drwx------. 2 admin admin  83 Sep 15 11:52 .
drwxr-xr-x. 3 root  root   19 Sep 15 11:41 ..
-rw-------. 1 admin admin 699 Oct 10 13:03 .bash_history
-rw-r--r--. 1 admin admin  18 Jun 17 01:42 .bash_logout
-rw-r--r--. 1 admin admin 141 Jun 17 01:42 .bash_profile
-rw-r--r--. 1 admin admin 376 Jun 17 01:42 .bashrc
```

## 🌞 Changer l'utilisateur utilisé par Apache

### utilisez une commande ps pour vérifier que le changement a pris effet


```
[admin@web ~]$ ps -ef
web.tp2    14216   14215  0 11:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web.tp2     14217   14215  0 11:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web.tp2     14218   14215  0 11:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
web.tp2     14219   14215  0 11:03 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
```

## 🌞 Faites en sorte que Apache tourne sur un autre port

```
[admin@web ~]$ sudo ss -l -p -n -t
State                    Recv-Q                   Send-Q                                       Local Address:Port                                       Peer Address:Port                   Process
LISTEN                   0                        128                                                0.0.0.0:22                                              0.0.0.0:*                       users:(("sshd",pid=834,fd=5))
LISTEN                   0                        128                                                   [::]:22                                                 [::]:*                       users:(("sshd",pid=834,fd=7))
LISTEN                   0                        128                                                      *:81                                                    *:*                       users:(("httpd",pid=14831,fd=4),("httpd",pid=148
```
```

PS C:\Users\Muralee> curl http://10.102.1.11:81
curl : HTTP Server Test Page
This page is used to test the proper operation of an HTTP server after it has been installed on a Rocky Linux system.
If you can read this page, it means that the software it working correctly.
Just visiting?
This website you are visiting is either experiencing problems or could be going through maintenance.
If you would like the let the administrators of this website know that you've seen this page instead of the page
you've expected, you should send them an email. In general, mail sent to the name "webmaster" and directed to the
website's domain should reach the appropriate person.
The most common email address to send to is: "webmaster@example.com"
```

## II. Une stack web plus avancée

### A. Serveur Web et NextCloud

### 🌞 Install du serveur Web et de NextCloud sur web.tp2.linux... 
### je veux dans le rendu toutes les commandes réalisées

``` 
[admin@web config]$ history
   97  sudo dnf install epel-release
   98  sudo dnf update
   99  sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
  100  sudo dnf module list php
  101  sudo dnf module enable php:remi-7.4
  102  sudo dnf module list php
  103  sudo dnf install httpd mariadb-server vim wget zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp -y
  104  systemctl enable httpd
  105  sudo nano /etc/httpd/sites-available/linux.tp2.web
  107  sudo mkdir /etc/httpd/sites-enabled
  108  sudo mkdir /etc/httpd/sites-available
  110  sudo nano /etc/httpd/sites-available/linux.tp2.web
  111  sudo nano /etc/httpd/conf/httpd.conf 
  116  ln -s /etc/httpd/sites-available/linux.tp2.web /etc/httpd/sites-enabled/
  117  ls /etc/httpd/sites-enabled/
  122  sudo mkdir -p /var/www/sub-domains/linux.tp2.web/html
  126  timedatectl
  128  sudo nano /etc/opt/remi/php74/php.ini
  129  ls -al /etc/localtime
  131 sudo wget https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
  132  unzip nextcloud-21.0.1.zip 
  133  cd nextcloud
  134  cp -Rf * /var/www/sub-domains/linux.tp2.web/html/
  137  chown -Rf apache.apache /var/www/sub-domains/linux.tp2.web/html
  139  ls /var/www/sub-domains/linux.tp2.web/ -larth
  141  systemctl restart httpd
  142  history

  ``` 
  
  ## B. Base de données
  ### 🌞 Install de MariaDB sur db.tp2.linux
  ### 🌞 Préparation de la base pour NextCloud
  
  ```
[admin@db ~]$ history
 35  sudo dnf install mariadb-server
 36  sudo systemctl enable mariadb
 37  sudo systemctl start mariadb
 38  sudo systemctl status mariadb
 39  sudo mysql -u root
 41  sudo firewall-cmd --add-port=3306/tcp --permanent
 42  sudo firewall-cmd --reload
 44  history
```


- connectez-vous à la base de données à l'aide de la commande
- exécutez les commandes SQL suivantes :
```
sudo mysql -u root  

CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'meow';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
FLUSH PRIVILEGES;
```

## 🌞 Exploration de la base de données

### connectez vous en ligne de commande à la base de données après l'installation terminée

```
[admin@web sites-available]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password:

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.001 sec)

MariaDB [(none)]> USE nextcloud;
MariaDB [nextcloud]> SHOW TABLES;
+-----------------------------+
| Tables_in_nextcloud         |
+-----------------------------+
| oc_accounts                 |
| oc_accounts_data            |
| oc_activity                 |
| oc_activity_mq              |
| oc_addressbookchanges       |
| oc_addressbooks             |
           [...]
| oc_vcategory                |
| oc_vcategory_to_object      |
| oc_webauthn                 |
| oc_whats_new                |
+-----------------------------+
77 rows in set (0.001 sec) 

```

### 77 tables ont été crée apres l'installation 
` 77 rows in set (0.001 sec) `

## Tableau

| Machine         | IP            | Service                 | Port ouvert | IP autorisées |
|-----------------|---------------|-------------------------|-------------|---------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             | 80          | x             |
| `db.tp2.linux ` | `10.102.1.12` | Serveur Base de donnée  | 3306        | 10.102.1.11   |


