# TP1 : (re)Familiaration avec un système GNU/Linux


## 0. Préparation de la machine

- Setup de deux machines Rocky Linux configurées de façon basique.

Voici les configurations des deux machines : 


        Machine 1 

        [admin@localhost ~]$ cd /etc/sysconfig/network-scripts/
        [admin@localhost network-scripts]$ sudo nano ifcfg-enp0s8


        TYPE=Ethernet
        PROXY_METHOD=none
        BROWSER_ONLY=no
        BOOTPROTO=static
        DEFROUTE=yes
        IPADDR=10.101.1.11
        NETMASK=255.255.255.0
        DNS1=1.1.1.1
        IPV4_FAILURE_FATAL=no
        IPV6INIT=yes
        IPV6_AUTOCONF=yes
        IPV6_DEFROUTE=yes
        IPV6_FAILURE_FATAL=no
        NAME=enp0s8
        UUID=f5ec2a49-4b10-499a-8575-f28349c2d537
        DEVICE=enp0s8
        ONBOOT=yes

        ---------------------------------------------------------------

        Machine 2 :

        [admin@node2 ~]$ cd /etc/sysconfig/network-scripts/
        [admin@node2 network-scripts]$ sudo nano ifcfg-enp0s8

        TYPE=Ethernet
        PROXY_METHOD=none
        BROWSER_ONLY=no
        BOOTPROTO=static
        DEFROUTE=yes
        IPADDR=10.101.1.12
        NETMASK=255.255.255.0
        DNS1=1.1.1.1
        IPV4_FAILURE_FATAL=no
        IPV6INIT=yes
        IPV6_AUTOCONF=yes
        IPV6_DEFROUTE=yes
        IPV6_FAILURE_FATAL=no
        NAME=enp0s8
        UUID=f5ec2a49-4b10-499a-8575-f28349c2d537
        DEVICE=enp0s8
        ONBOOT=yes




- Un accès à un réseau local (les deux machines peuvent se ping) (via la carte Host-Only)

![](https://i.imgur.com/86Liine.png)


    ```


    [admin@localhost~]$ ping 10.101.1.12
    PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
    64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=1.21 ms
    64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.750 ms
    64 bytes from 10.101.1.12: icmp_seq=3 ttl=64 time=0.985 ms
    64 bytes from 10.101.1.12: icmp_seq=4 ttl=64 time=1.01 ms
    64 bytes from 10.101.1.12: icmp_seq=5 ttl=64 time=1.01 ms
    ^C
    --- 10.101.1.12 ping statistics ---
    5 packets transmitted, 5 received, 0% packet loss, time 4014ms
    rtt min/avg/max/mdev = 0.750/0.994/1.212/0.149 ms
 

    __________________________________________________________________________

    [admin@node2 /]$ ping 10.101.1.11
    PING 10.101.1.11 (10.101.1.11) 56(84) bytes of data.
    64 bytes from 10.101.1.11: icmp_seq=1 ttl=64 time=1.05 ms
    64 bytes from 10.101.1.11: icmp_seq=2 ttl=64 time=0.982 ms
    64 bytes from 10.101.1.11: icmp_seq=3 ttl=64 time=1.10 ms
    64 bytes from 10.101.1.11: icmp_seq=4 ttl=64 time=1.08 ms
    64 bytes from 10.101.1.11: icmp_seq=5 ttl=64 time=1.20 ms
    ^C
    --- 10.101.1.11 ping statistics ---
    5 packets transmitted, 5 received, 0% packet loss, time 4007ms
    rtt min/avg/max/mdev = 0.982/1.081/1.197/0.078 ms
 
    ```
- donnez un acces internet
 ```
 [admin@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=111 time=121 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=41.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=38.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=41.1 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 38.438/60.549/121.130/34.996 ms

_____

 [admin@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=2 ttl=111 time=31.6 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=111 time=28.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=111 time=31.1 ms

--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 38.438/60.549/121.130/34.996 ms

 ```


- vous n'utilisez QUE ssh pour administrer les machines

    ```
    Windows PowerShell
    Copyright (C) Microsoft Corporation. Tous droits réservés.

    Testez le nouveau système multiplateforme PowerShell https://aka.ms/pscore6

    PS C:\Users\Muralee> ssh admin@10.101.1.12
    The authenticity of host '10.101.1.12 (10.101.1.12)' can't be established.
    ECDSA key fingerprint is SHA256:+vqnJbKDsMSWOp9Wkk10Y+7izGqLKNLKXgNL+f4tD5E.
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '10.101.1.12' (ECDSA) to the list of known hosts.
    admin@10.101.1.12's password:
    Activate the web console with: systemctl enable --now cockpit.socket

    Last login: Wed Sep 22 11:35:38 2021

    ______________________________________________________________________________________

    Windows PowerShell
    Copyright (C) Microsoft Corporation. Tous droits réservés.

    Testez le nouveau système multiplateforme PowerShell https://aka.ms/pscore6

    PS C:\Users\Muralee> ssh admin@10.101.1.11
    The authenticity of host '10.101.1.11 (10.101.1.11)' can't be established.
    ECDSA key fingerprint is SHA256:+vqnJbKDsMSWOp9Wkk10Y+7izGqLKNLKXgNL+f4tD5E.
    Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
    Warning: Permanently added '10.101.1.11' (ECDSA) to the list of known hosts.
    admin@10.101.1.11's password:
    Activate the web console with: systemctl enable --now cockpit.socket

    Last login: Wed Sep 22 11:36:07 2021


    ```


- les machines doivent avoir un nom

Section dédiée du mémo réseau
les noms que doivent posséder vos machines sont précisés dans le tableau plus bas

   
    Machine 1 : 

    [admin@localhost ~]$ sudo hostname node1.tp1.b2
    [sudo] password for admin:
    [admin@localhost ~]$ hostname
    node1.tp1.b2

    __________________________________________

    Machine 2 :

    [admin@localhost /]$ sudo hostname node2.tp1.b2
    [sudo] password for admin:
    [admin@llocalhost /]$ hostname
    node2.tp1.b2






- Utiliser 1.1.1.1 comme serveur DNS

Section dédiée du mémo réseau
vérifier avec le bon fonctionnement avec la commande dig

avec dig, demander une résolution du nom ynov.com




    [admin@localhost ~]$ dig ynov.com

    ; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> ynov.com
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 251
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ;; QUESTION SECTION:
    ;ynov.com.                      IN      A

    ;; ANSWER SECTION:
    ynov.com.               7466    IN      A       92.243.16.143**

    ;; Query time: 23 msec
    ;; SERVER: 1.1.1.1#53(1.1.1.1)
    ;; WHEN: Wed Sep 22 12:39:58 CEST 2021
    ;; MSG SIZE  rcvd: 53
    
    _______________________________________________________________________
    
    l'adresse de ynov : 92.243.16.143 
    l'adresse du serveur : 192.168.1.1




- les machines doivent pouvoir se joindre par leurs noms respectifs

        [admin@localhost ~]$ ping node2.tp1.b2

        PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
        64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=2.86 ms
        64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=1.04 ms
        64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=4 ttl=64 time=0.684 ms

        --- node2.tp1.b2 ping statistics ---
        3 packets transmitted, 4 received, 0% packet loss, time 5011ms
        rtt min/avg/max/mdev = 0.684/1.264/2.859/0.744 ms





        [admin@node2 ~]$ ping node1.tp1.b2
        
        

        PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=1.28 ms
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.712 ms
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=4 ttl=64 time=0.801 ms
    
        --- node1.tp1.b2 ping statistics ---
        3 packets transmitted, 4 received, 0% packet loss, time 3007ms
        rtt min/avg/max/mdev = 0.712/1.006/1.282/0.205 ms
        
- le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires

commande firewall-cmd

        
```

    [admin@localhost usr]$ sudo firewall-cmd --list-all
    public (active)
      target: default
      icmp-block-inversion: no
      interfaces: enp0s3 enp0s8
      sources:
      services: ssh
      ports: 22/tcp
       protocols:
      masquerade: no
      forward-ports:
      source-ports:
      icmp-blocks:
      rich rules:

```
  
        
        
## I. Utilisateurs

### Création et configuration

- Ajouter un utilisateur à la machine, qui sera dédié à son administration. Précisez des options sur la commande d'ajout pour que :
    
le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans /home

le shell de l'utilisateur soit /bin/bash
    
 ```
  [admin@localhost ~]$ sudo useradd mukesh -d /home/ -s /bin/bash -u 100
[sudo] password for admin:


[admin@localhost ~]$ cat /etc/passwd
...
mukesh:x:100:1001::/home/:/bin/bash

________________________________________________________________________


[admin@node2 ~]$ sudo useradd mukesh2.0 -d /home/ -s /bin/bash -u 100
[sudo] password for admin: 


[admin@node2 ~]$ cat /etc/passwd
...
mukesh2.0:x:100:1001::/home/:/bin/bash

```

- Créer un nouveau groupe admins qui contiendra les utilisateurs de la machine ayant accès aux droits de root via la commande sudo.

 il faut modifier le fichier /etc/sudoers
 
on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
 
```
[admin@localhost ~]$ sudo groupadd admins
[admin@localhost ~]$ sudo visudo
%admins ALL=(ALL) ALL

```
        
- Ajouter votre utilisateur à ce groupe admins

Utilisateur créé et configuré
Groupe admins créé
Groupe admins ajouté au fichier /etc/sudoers
Ajout de l'utilisateur au groupe admins
 
```
[admin@localhost ~]$ sudo usermod -aG admins mukesh
[admin@localhost ~]$ groups mukesh
mukesh : mukesh admins

```

  
## 2. SSH

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine

- Il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )


```
- génération de clé depuis VOTRE poste donc
sur Windows, on peut le faire avec le programme puttygen.exe qui est livré avec putty.exe



- déposer la clé dans le fichier /home/<USER>/.ssh/authorized_keys de la machine que l'on souhaite administrer

- vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP

- on peut le faire à la main
ou avec la commande ssh-copy-id

```

```

C:\Users\muralee> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\marti/.ssh/id_rsa):
C:\Users\muralee/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\muralee/.ssh/id_rsa.
Your public key has been saved in C:\Users\muralee/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:onWG7Wt8U+k4ux0jgQkluR0VmEn4TBge/+HYCwnsMn4 marti@Pc-Martial
The key's randomart image is:
+---[RSA 4096]----+
|      +*o=o.     |
|     o+=*        |
|      =*...      |
|     ..=+O .     |
|    o = S =  .   |
|   . = = . oo    |
|    o E.. o+o    |
|     .  o.=o.o   |
|       ...o=.    |
+----[SHA256]-----+
C:\Users\muralee> ssh admin@10.101.1.11
admin@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Sep 27 18:50:21 2021 from 10.101.1.1

_____________________________________________________________________

C:\Users\muralee> ssh admin@10.101.1.11
admin@10.101.1.11's password:
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Sep 27 20:50:44 2021 from 10.101.1.1


```



## II. Partitionnement


- Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

- Agréger les deux disques en un seul volume group

```

[admin@localhost ~]$ sudo pvcreate /dev/sdb
[sudo] password for admin:
  Physical volume "/dev/sdb" successfully created.
[admin@localhost ~]$ sudo pvcreate /dev/sdc
  Physical volume "/dev/sdc" successfully created.
  
[admin@localhost ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <8.00g    0
  /dev/sdb      lvm2 ---   3.00g 3.00g
  /dev/sdc      lvm2 ---   3.00g 3.00g
  (...)
 
  
```

- créer 3 logical volumes de 1 Go chacun 


```

[admin@localhost ~]$ sudo lvcreate -L 1G data -n lv_data_1
  Logical volume "lv_data_1" created.
[admin@localhost ~]$ sudo lvcreate -L 1G data -n lv_data_2
  Logical volume "lv_data_2" created.
[admin@localhost ~]$ sudo lvcreate -L 1G data -n lv_data_3
  Logical volume "lv_data_3" created.
  
  
[admin@localhost ~]$ sudo lvs
  LV        VG   Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  lv_data_1 data -wi-a-----   1.00g
  lv_data_2 data -wi-a-----   1.00g
  lv_data_3 data -wi-a-----   1.00g
  root      rl   -wi-ao----  <6.20g
  swap      rl   -wi-ao---- 820.00m
  
```

- formater ces partitions en ext4

```

    [admin@localhost ~]$ sudo mkfs -t ext4 /dev/data/lv_data_1
    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: b3dade83-99da-42f3-9c83-b017fe4aae78
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done
                          ______________________
    
    
    [admin@localhost ~]$ sudo mkfs -t ext4 /dev/data/lv_data_2
    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: eb9aa6a7-2048-461c-9582-cc355ec32741
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done
    
                          ______________________    
    
    
    [admin@localhost ~]$ sudo mkfs -t ext4 /dev/data/lv_data_3
    mke2fs 1.45.6 (20-Mar-2020)
    Creating filesystem with 262144 4k blocks and 65536 inodes
    Filesystem UUID: b2f371ff-7488-4315-8335-7633fa708a4e
    Superblock backups stored on blocks:
            32768, 98304, 163840, 229376

    Allocating group tables: done
    Writing inode tables: done
    Creating journal (8192 blocks): done
    Writing superblocks and filesystem accounting information: done
```

- monter ces partitions pour qu'elles soient accessibles aux points de montage /mnt/part1, /mnt/part2 et /mnt/part3.
```

[admin@localhost ~]$ sudo mkdir /mnt/part1
[admin@localhost ~]$ sudo mount /dev/data/lv_data_1 /mnt/part1

[admin@localhost ~]$ sudo mkdir /mnt/part2
[admin@localhost ~]$ sudo mount /dev/data/lv_data_2 /mnt/part2

[admin@localhost ~]$ sudo mkdir /mnt/part3
[admin@localhost ~]$ sudo mount /dev/data/lv_data_3 /mnt/part3

```


- Grâce au fichier /etc/fstab, faites en sorte que cette partition soit montée automatiquement au démarrage du système.


```

[admin@localhost ~]$ sudo nano /etc/fstab

/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=54c1c5d3-e7b8-49ce-84d7-a304a94eb447 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
/dev/data/lv_data_1 /mnt/part1 ext4 defaults 0 0
/dev/data/lv_data_2 /mnt/part2 ext4 default 0 0
/dev/data/lv_data_3 /mnt/part3 ext4 default 0 0

```

## III. Gestion de services

Au sein des systèmes GNU/Linux les plus utilisés, c'est systemd qui est utilisé comme gestionnaire de services (entre autres).
Pour manipuler les services entretenus par systemd, on utilise la commande systemctl.
On peut lister les unités systemd actives de la machine systemctl list-units -t service.

Beaucoup de commandes systemd qui écrivent des choses en sortie standard sont automatiquement munie d'un pager (pipé dans less). On peut ajouter l'option --no-pager pour se débarasser de ce comportement

Pour obtenir plus de détails sur une unitée donnée




- détermine si l'unité est actuellement en cours de fonctionnement


```
[admin@localhost ~]$ systemctl is-active firewalld
active

```


- détermine si l'unité est activée ("activée" = démarre au boot)

```
[admin@localhost ~]$ systemctl is-enabled firewalld
enabled

```


## 2. Création de service

Uniquement sur node1.tp1.b2.

#### A. Unité simpliste


```

[admin@localhost ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[admin@localhost ~]$ sudo systemctl status web
    web.service - Very simple web service
   Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: dis>
   Active: active (running) since Sun 2021-09-27 17:00:42 CEST; 1s ago
   
```


####  B. Modification de l'unité

🌞 Créer un utilisateur web.
```
[admin@localhost ~]$ sudo useradd web

```
🌞 Modifiez l'unité de service web.service créée précédemment en ajoutant les clauses :


```
[admin@localhost ~]$ sudo nano /etc/systemd/system/web.service
[Unit]
Description=Very simple web service

[Service]
ExecStart=/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/srv/srvweb

```

- Placer un fichier de votre choix dans le dossier créé dans /srv et tester que vous pouvez y accéder une fois le service actif. Il faudra que le dossier et le fichier qu'il contient appartiennent à l'utilisateur web.

`[admin@localhost ~]$ touch /srv/test`



- Vérifier le bon fonctionnement avec une commande curl

```

PS C:\Users\muralee> curl http://10.101.1.11:8888/srv/


StatusCode        : 200
StatusDescription : OK
Content           : test 42

RawContent        : HTTP/1.0 200 OK
                    Content-Length: 8
                    Content-Type: text/plain
                    Date: Mon, 27 Sep 2021 22:37:41 GMT
                    Last-Modified: Sun, 26 Sep 2021 22:34:13 GMT
                    Server: SimpleHTTP/0.6 Python/3.6.8

                    test 42
    (...)
    
```
